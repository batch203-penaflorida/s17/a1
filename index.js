/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
function printUserInfo() {
  let getName = prompt("Enter your fullname: ");

  let getAge = prompt("Enter your age: ");

  let getLocation = prompt("Enter your location: ");
  console.log("Hello " + getName);
  console.log("You are " + getAge + " years old.");
  console.log("You live in " + getLocation);
}
printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
function printFavoriteBands() {
  const favBands = [
    "Lola Amour",
    "Linkin Park",
    "Eraserheads",
    "AC/DC",
    "Rivermaya",
  ];
  console.log(`1. ${favBands[0]}`);
  console.log(`2. ${favBands[1]}`);
  console.log(`3. ${favBands[2]}`);
  console.log(`4. ${favBands[3]}`);
  console.log(`5. ${favBands[4]}`);
}
printFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
function printFavoriteMovies() {
  let favMovie1 =
    "1. Lord of the Rings: The Fellowship of the Ring \nRotten Tomatoes Rating: 91%";
  console.log(favMovie1);
  let favMovie2 =
    "2. Lord of the Rings: The Two Towers \nRotten Tomatoes Rating: 95%";
  console.log(favMovie2);
  let favMovie3 =
    "3. Lord of the Rings: The Return of the King \nRotten Tomatoes Rating: 93%";
  console.log(favMovie3);
  let favMovie4 = "4. John Wick \nRotten Tomatoes Rating: 86%";
  console.log(favMovie4);
  let favMovie5 = "5. John Wick: Chapter 2 \nRotten Tomatoes Rating: 89%";
  console.log(favMovie5);
}
printFavoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with: ");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

// console.log(friend1); // outside the scope - error
// console.log(friend2); // outside the scope - error
printFriends();
